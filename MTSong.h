//
//  MTSong.h
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.25.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MTSong : NSManagedObject

@property (nonatomic, retain) NSNumber * songID;
@property (nonatomic, retain) NSNumber * moodID;
@property (nonatomic, retain) NSString * pastMoods;

@end

//
//  MTSong.m
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.25.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "MTSong.h"


@implementation MTSong

@dynamic songID;
@dynamic moodID;
@dynamic pastMoods;

@end

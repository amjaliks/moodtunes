//
//  MTPlay.m
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.24.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "MTPlay.h"


@implementation MTPlay

@dynamic songID;
@dynamic datetime;
@dynamic skipped;

@end

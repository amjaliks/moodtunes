//
//  MTPlay.h
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.24.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MTPlay : NSManagedObject

@property (nonatomic, retain) NSNumber * songID;
@property (nonatomic, retain) NSDate * datetime;
@property (nonatomic, retain) NSNumber * skipped;

@end

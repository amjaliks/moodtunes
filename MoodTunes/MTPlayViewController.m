	//
//  MTPlayViewController.m
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.25.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "MTLibraryManager.h"
#import "MTPlayViewController.h"

@interface MTPlayViewController ()

@end

@implementation MTPlayViewController

@synthesize artworkView;
@synthesize titleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(nowPlayingItemDidChange:)
												 name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
											   object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self nowPlayingItemDidChange:nil];
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender
{
	[self dismissModalViewControllerAnimated:NO];
}

- (IBAction)skip:(id)sender
{
	[[MTLibraryManager sharedManager] skipNowPlayingItem];
}

- (void)nowPlayingItemDidChange:(id)sender
{
	MPMediaItem *song = [[[MTLibraryManager sharedManager] iPodMusicPlayer] nowPlayingItem];
	if (song) {
		MPMediaItemArtwork *artwork = [song valueForKey:MPMediaItemPropertyArtwork];
		artworkView.image = [artwork imageWithSize:artworkView.bounds.size];
		
		titleLabel.text = [NSString stringWithFormat:@"%@ - %@",
						   [song valueForKey:MPMediaItemPropertyArtist],
						   [song valueForKey:MPMediaItemPropertyTitle]];
	}
}

@end

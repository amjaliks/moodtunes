//
//  MTLibraryManager.m
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.25.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MTLibraryManager.h"
#import "MTSong.h"

@interface MTLibraryManager () {
	NSManagedObjectModel *managedObjectModel;
	NSManagedObjectContext *managedObjectContext;
	NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@end


@implementation MTLibraryManager

@synthesize iPodMusicPlayer;

+ (MTLibraryManager *)sharedManager
{
	static MTLibraryManager *instance;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[MTLibraryManager alloc] init];
	});
	
	return instance;
}

- (id)init
{
	self = [super init];
	if (self) {
		iPodMusicPlayer = [MPMusicPlayerController iPodMusicPlayer];
		[iPodMusicPlayer beginGeneratingPlaybackNotifications];
	}
	return self;
}

- (void)scanLibrary
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSArray *albums = [[MPMediaQuery songsQuery] collections];
		for (MPMediaItemCollection *album in albums) {
			NSArray *songs = [album items];
			for (MPMediaItem *song in songs) {
				NSNumber *persistenID = [song valueForProperty:MPMediaItemPropertyPersistentID];
				MTSong *songProfile = [self songProfileWithPersistentID:persistenID];
				if (!songProfile) {
					[self createSongProfileWithPersistentID:persistenID];
					[self commit];
				}
			}
		}
	});
}

- (MTSong *)songProfileWithPersistentID:(NSNumber *)persistentID
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"Song" inManagedObjectContext:self.managedObjectContext]];
	[request setPredicate:[NSPredicate predicateWithFormat:@"(songID == %@)", persistentID]];

	NSArray *array = [managedObjectContext executeFetchRequest:request error:nil];
	return [array lastObject];
}

- (MTSong *)createSongProfileWithPersistentID:(NSNumber *)persistentID
{
	MTSong *song = [NSEntityDescription insertNewObjectForEntityForName:@"Song" inManagedObjectContext:self.managedObjectContext];
	song.songID = persistentID;
	song.moodID = [NSNumber numberWithInt:arc4random() % 6 + 1];
	
	return song;
}

- (void)playSongWithMoodID:(NSNumber *)moodID
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:@"Song" inManagedObjectContext:self.managedObjectContext]];
	[request setPredicate:[NSPredicate predicateWithFormat:@"(moodID == %@)", moodID]];
	
	NSMutableArray *songs = [[managedObjectContext executeFetchRequest:request error:nil] mutableCopy];
	NSMutableArray *playlist = [NSMutableArray arrayWithCapacity:[songs count]];
	
	while ([songs count]) {
		MTSong *song = [songs objectAtIndex:arc4random() % [songs count]];
		
		NSArray *albums = [[[MPMediaQuery alloc] initWithFilterPredicates:[NSSet setWithObject:[MPMediaPropertyPredicate predicateWithValue:song.songID forProperty:MPMediaItemPropertyPersistentID]]] collections];
		
		for (MPMediaItemCollection *album in albums) {
			[playlist addObjectsFromArray:[album items]];
		}
		
		[songs removeObject:song];
	}
	
	[iPodMusicPlayer stop];	
	[iPodMusicPlayer setQueueWithItemCollection:[MPMediaItemCollection collectionWithItems:playlist]];
	[iPodMusicPlayer play];
}

- (void)skipNowPlayingItem
{
	NSTimeInterval currentPlaybackTime = [iPodMusicPlayer currentPlaybackTime];
	
	MPMediaItem *item = [iPodMusicPlayer nowPlayingItem];
	[iPodMusicPlayer skipToNextItem];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSNumber *persistentID = [item valueForProperty:MPMediaItemPropertyPersistentID];
 		NSTimeInterval playbackDuration = [[item valueForProperty:MPMediaItemPropertyPlaybackDuration] doubleValue];
		
		if (currentPlaybackTime / playbackDuration < 0.25f) {
			MTSong *song = [self songProfileWithPersistentID:persistentID];
			
			if (song) {
				NSString *pastMoods = song.pastMoods;
				NSInteger moodID = [song.moodID integerValue];
				
				NSInteger possibleMoods[5];
				NSInteger possibleMoodCount = 0;
				
				if ([pastMoods length] == 5) {
					for (NSInteger c = 1; c <= 6; c++) {
						if (c != moodID) {
							possibleMoods[possibleMoodCount] = c;
							possibleMoodCount++;
						}
					}
					pastMoods = @"";
				} else {
					for (NSInteger c = 1; c <= 6; c++) {
						NSString *digit = [NSString stringWithFormat:@"%d", c];
						if ([pastMoods rangeOfString:digit].location != NSNotFound) {
							possibleMoods[possibleMoodCount] = c;
							possibleMoodCount++;
						}
					}
					pastMoods = [pastMoods stringByAppendingFormat:@"%d", moodID];
				}
				
				song.moodID = [NSNumber numberWithInteger:possibleMoods[arc4random() % possibleMoodCount]];
				song.pastMoods = pastMoods;
				
				[self commit];
			}
		}
	});	
}

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
	
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [NSManagedObjectContext new];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Model" ofType:@"momd"];
    NSURL *momURL = [NSURL fileURLWithPath:path];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
    
    return managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
	
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Library.sqlite"];
	
	NSString *storePath = [[self applicationDocumentDirectory] stringByAppendingPathComponent:@"Library.sqlite"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:storePath]) {
        [[NSFileManager defaultManager] moveItemAtPath:storePath toPath:path error:nil];
    }
    
	//	/*
	//	 Set up the store.
	//	 For the sake of illustration, provide a pre-populated default store.
	//	 */
	//	NSFileManager *fileManager = [NSFileManager defaultManager];
	//	// If the expected store doesn't exist, copy the default store.
	//	if (![fileManager fileExistsAtPath:storePath]) {
	//		NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"Recipes" ofType:@"sqlite"];
	//		if (defaultStorePath) {
	//			[fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
	//		}
	//	}
	
	NSURL *storeUrl = [NSURL fileURLWithPath:path];
	
	NSError *error;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    //Turn on automatic store migration
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
	
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
		 
		 Typical reasons for an error here include:
		 * The persistent store is not accessible
		 * The schema for the persistent store is incompatible with current managed object model
		 Check the error message to determine what the actual problem was.
		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
    }    
	
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)commit
{
	[managedObjectContext save:nil];
}

@end

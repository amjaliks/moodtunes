//
//  main.m
//  Mune
//
//  Created by Aleksejs Mjaliks on 12.03.24.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MTAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MTAppDelegate class]));
	}
}

//
//  MTMainViewController.m
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.24.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "MTLibraryManager.h"
#import "MTMainViewController.h"

@interface MTMainViewController ()

@end

@implementation MTMainViewController

@synthesize playViewController;
@synthesize previewView;
@synthesize scanOverlayView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)play:(id)sender
{
	NSInteger tag = [sender tag];
	[[MTLibraryManager sharedManager] playSongWithMoodID:[NSNumber numberWithInteger:tag]];
	
	[self presentModalViewController:playViewController animated:NO];
}

- (IBAction)scan:(id)sender
{
	scanOverlayView.hidden = NO;
	previewView.hidden = NO;
	
	[self startScanning];
}

- (void)startScanning
{
	// seting
	AVCaptureSession *session = [[AVCaptureSession alloc] init];
	
	// using highest available quality
	session.sessionPreset = AVCaptureSessionPreset640x480;

	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	AVCaptureInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
	[session addInput:input];
	
	AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
	[session addOutput:output];
	output.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
													   forKey:(id)kCVPixelBufferPixelFormatTypeKey];
	dispatch_queue_t queue = dispatch_queue_create("MyQueue", NULL);
	[output setSampleBufferDelegate:self queue:queue];
	dispatch_release(queue);
	
	[session startRunning];
	
	// setup preview
	CALayer *layer = previewView.layer;
	
	AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
	captureVideoPreviewLayer.frame = previewView.bounds;
	[layer addSublayer:captureVideoPreviewLayer];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
	UIImage *image = imageFromSampleBuffer(sampleBuffer);
}

UIImage *imageFromSampleBuffer(CMSampleBufferRef sampleBuffer) {
	
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer.
    CVPixelBufferLockBaseAddress(imageBuffer,0);
	
    // Get the number of bytes per row for the pixel buffer.
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height.
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
	
	size_t bytesPerPixel = bytesPerRow / width;
	size_t offset = (height / 2 - 3) * bytesPerRow + (width / 2 - 3) * bytesPerRow / width;
	
    // Create a device-dependent RGB color space.
    static CGColorSpaceRef colorSpace = NULL;
    if (colorSpace == NULL) {
        colorSpace = CGColorSpaceCreateDeviceRGB();
		if (colorSpace == NULL) {
            // Handle the error appropriately.
            return nil;
        }
    }
	
    // Get the base address of the pixel buffer.
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
	
	int colours[3] = {0, 0, 0};
	for (size_t r = 0; r < 6; r++) {
		for (size_t c = 0; c < 6; c++) {
			unsigned char * pixel = (unsigned char *) (baseAddress + offset + bytesPerPixel * r + c * 4);
			colours[0] += (unsigned char) pixel[2];
			colours[1] += (unsigned char) pixel[1];
			colours[2] += (unsigned char) pixel[0];
		}
	}
	
	unsigned char colour[3];
	colour[0] = colours[0] / 36;
	colour[1] = colours[1] / 36;
	colour[2] = colours[2] / 36;
	
	float mult = 255.0f / MAX(colour[0], MAX(colour[1], colour[2]));
	
	colour[0] = (int) (colour[0] * mult / 64.0f);
	colour[1] = (int) (colour[1] * mult / 64.0f);
	colour[2] = (int) (colour[2] * mult / 64.0f); 
	
	static unsigned char samples[6][3] = {
		{3, 0, 0},
		{0, 3, 0},
		{3, 3, 0},
		{0, 0, 3},
		{3, 0, 3},
		{3, 1, 0}
	};
	
	NSLog(@"%d, %d, %d", colours[0] / 36, colours[1] / 36, colours[2] / 36);
	
	for (int i = 0; i < 6; i++) {
		
		
		if (mult < 2.0f) {
		NSLog(@"c: %d, %d, %d, %d, %@", i, colour[0], colour[1], colour[2], 
			  (colour[0] == samples[i][0]
				&& colour[1] == samples[i][1]
				&& colour[2] == samples[i][2] ? @"yes" : @"no"));
		}
	}
	
    return nil;
}

@end

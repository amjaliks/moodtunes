//
//  MTMainViewController.h
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.24.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface MTMainViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate> {

}

@property (nonatomic, strong) IBOutlet UIViewController *playViewController;
@property (nonatomic, strong) IBOutlet UIView *previewView;
@property (nonatomic, strong) IBOutlet UIImageView *scanOverlayView;

- (IBAction)play:(id)sender;
- (IBAction)scan:(id)sender;

@end

//
//  MTPlayViewController.h
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.25.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTPlayViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView *artworkView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

- (IBAction)back:(id)sender;
- (IBAction)skip:(id)sender;

@end

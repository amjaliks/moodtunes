//
//  MTLibraryManager.h
//  MoodTunes
//
//  Created by Aleksejs Mjaliks on 12.03.25.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MPMusicPlayerController;

@interface MTLibraryManager : NSObject

@property (nonatomic) MPMusicPlayerController *iPodMusicPlayer;

+ (MTLibraryManager *)sharedManager;

- (void)scanLibrary;
- (void)playSongWithMoodID:(NSNumber *)moodID;
- (void)skipNowPlayingItem;

@end
